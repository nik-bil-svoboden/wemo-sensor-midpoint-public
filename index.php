<?php

date_default_timezone_set('Europe/Kiev');

$_ = $_GET;

header("HTTP/1.1 200 OK");

if ($_ && $_['sensor_id'] && $_['auth_key'] && $_['task']) {
    $client = new GearmanClient();
    $client->addServer();
    $client->doBackground("post", json_encode([
        "id" => $_['sensor_id'],
        "auth_key" => $_['auth_key'],
        "task" => $_['task'],
        "time" => date('Y-m-d H:i:s')
    ]));
    echo 'OK';
} else {
    echo 'ERROR';
}
