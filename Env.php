<?php

class Env
{
    private $env = [];

    private static $instance = null;

    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone() {
    }

    private function __construct() {
        $env_file = trim(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . ".env"));

        $env_array = explode("\n", $env_file);

        foreach ($env_array as $var) {
            list ($key, $val) = explode("=", $var);
            $val = trim($val, " \t\n\r\0\x0B\"");

            switch ($val) {
                case "true":
                    $this->env[$key] = true;
                    break;
                case "false":
                    $this->env[$key] = false;
                    break;
                default:
                    $this->env[$key] = $val;
                    break;
            }
        }
    }

    public function get($key, $default = null) {
        return array_key_exists($key, $this->env) ? $this->env[$key] : $default;
    }
}

function env($key, $default = null) {
    $env = Env::getInstance();
    return $env->get($key, $default);
}