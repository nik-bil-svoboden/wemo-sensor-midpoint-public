<?php
require_once "Env.php";

$worker = new GearmanWorker();
$worker->addServer();
$worker->addFunction("post", "post");
while ($worker->work()) ;

function post($job) {
    $workload = json_decode($job->workload());
    $data = [
        "id" => $workload->id,
        "auth_key" => $workload->auth_key,
        "task" => $workload->task,
        "time" => $workload->time
    ];

    $options = array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context = stream_context_create($options);
    file_get_contents(env("APP_URL"), false, $context);
}